/* Простой пример TCP-сервера для сервиса echo 
для обслуживания нескольких клиентов 
Компиляция: gcc TCPServerThread.c -o TCPServerThread -lpthread
Запуск: ./TCPServerThread
Константы COUNT_THREADS и MAX_QUEUE_CLIENTS должны совпадать,
иначе потеряется открытый второй (клиентский) сокет.
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define COUNT_THREADS 4
#define MAX_QUEUE_CLIENTS 4
#define TIMEOUT_ESTABLISH_CONN_SEC 20

struct param
{
    int* id;        // id потока, показывает что он занят
    int newsockfd;  // новый сокет
};

void* answer(void* arg) 
{
    char line[1000];       /* Буфер для приема информации */
    int n;                 /* Количество принятых символов */
    /* Время ожидания сервером запроса от клиента при уже установленном соединении */
    struct timeval timeout;
    timeout.tv_sec = TIMEOUT_ESTABLISH_CONN_SEC;
    timeout.tv_usec = 0;
	
    struct param p = *((struct param *)arg);

    printf("OK, myThreadId=%d\n", *(p.id));
    printf("Sleep 5 sec\n");
    sleep(5);
    
    /* Устанавливаем время ожидания для клиента при уже установленном соединении */
    if (setsockopt(p.newsockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
	      perror("Error to set timeout");
	
    if (setsockopt(p.newsockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("Error to set timeout");

    while((n = read(p.newsockfd, line, 999)) > 0 )
    {
        /* Печатаем принятый текст на экране */
        printf("Received message: %s\n", line);

        /* Принятые данные отправляем обратно */
        if( (n = write(p.newsockfd, line, strlen(line) + 1)) < 0 )
        {
            perror(NULL);
            close(p.newsockfd);
            //exit(1);
        }
    }

    /* Если при чтении возникла ошибка – завершаем работу */
    if(n < 0)
    {
        perror(NULL);
        close(p.newsockfd);
        //exit(1);
    }

    /* Закрываем дескриптор присоединенного сокета */
    close(p.newsockfd);

	  /* Осовобождаем поток */
    *(p.id) = 0;
}


int main()
{
    pthread_t threads[COUNT_THREADS];
    int threadid[COUNT_THREADS] = { 0 };
    struct param p[COUNT_THREADS];

    int sockfd, newsockfd; /* Дескрипторы для слушающего и присоединенного сокетов */
    int clilen;            /* Длина адреса клиента */
    int i;

    /* Структуры для размещения полных адресов сервера и клиента */
    struct sockaddr_in servaddr, cliaddr;
	
	
    /* Создаем TCP-сокет */
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror(NULL);
        exit(1);
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family= AF_INET;
    servaddr.sin_port= htons(51000);
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* Настраиваем адрес сокета */
    if(bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
    {
        perror(NULL);
        close(sockfd);
        exit(1);
    }


    if(listen(sockfd, MAX_QUEUE_CLIENTS) < 0)
    {
        perror(NULL);
        close(sockfd);
        exit(1);
    }

    /* Основной цикл сервера */
    while(1)
    {
        clilen = sizeof(cliaddr);

        printf("Listen (accept)\n");
        if((newsockfd = accept(sockfd, (struct sockaddr *) &cliaddr, &clilen)) < 0)
        {
            perror(NULL);
            close(sockfd);
            exit(1);
        }
		
        /* Необходимо использовать стековую структуру для поиска свободных потоков */
        for(i = 0; i < COUNT_THREADS; ++i)
        {
            if((threadid[i] == 0 ) && 1)
            {
                threadid[i] = i + 1;
                p[i].id = &threadid[i];
                p[i].newsockfd = newsockfd;
                
                pthread_create(&threads[i], NULL, answer, &p[i]);
                break;
            }
        }
    }

    return 0;
}	
