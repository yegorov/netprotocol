/* Простой пример TCP-сервера для сервиса echo 
для обслуживания нескольких клиентов 
Компиляция: gcc TCPServerFork.c -o TCPServerFork
Запуск: ./TCPServerFork
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#define MAX_QUEUE_CLIENTS 50
#define TIMEOUT_ESTABLISH_CONN_SEC 20

int main()
{
    int sockfd, newsockfd; /* Дескрипторы для слушающего и присоединенного сокетов */
    int clilen;            /* Длина адреса клиента */
    int n;                 /* Количество принятых символов */
    char line[1000];       /* Буфер для приема информации */
    /* Структуры для размещения полных адресов сервера и клиента */
    struct sockaddr_in servaddr, cliaddr;
	
    /* Время ожидания сервером запроса от клиента при уже установленном соединении */
    struct timeval timeout;
    timeout.tv_sec = TIMEOUT_ESTABLISH_CONN_SEC;
    timeout.tv_usec = 0;

    pid_t pid; /* PID порожденного процесса */

    /* Создаем TCP-сокет */
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror(NULL);
        exit(1);
    }

    /* Заполняем структуру для адреса сервера: семейство
    протоколов TCP/IP, сетевой интерфейс – любой, номер
    порта 51000. Поскольку в структуре содержится
    дополнительное не нужное нам поле, которое должно
    быть нулевым, побнуляем ее всю перед заполнением */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family= AF_INET;
    servaddr.sin_port= htons(51000);
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* Настраиваем адрес сокета */
    if(bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
    {
        perror(NULL);
        close(sockfd);
        exit(1);
    }

    /* Переводим созданный сокет 
    в пассивное (слушающее) состояние. 
    Глубину очереди для установленных соединений
    описываем значением MAX_QUEUE_CLIENTS */
    if(listen(sockfd, MAX_QUEUE_CLIENTS) < 0)
    {
        perror(NULL);
        close(sockfd);
        exit(1);
    }

    /* Основной цикл сервера */
    while(1)
    {
        /* В переменную clilen заносим максимальную
        длину ожидаемого адреса клиента */
        clilen = sizeof(cliaddr);

        /* Ожидаем полностью установленного соединения
        на слушающем сокете. При нормальном завершении
        у нас в структуре cliaddr будет лежать полный
        адрес клиента, установившего соединение, а в
        переменной clilen – его фактическая длина. Вызов
        же вернет дескриптор присоединенного сокета, через
        который будет происходить общение с клиентом.
        Заметим, что информация о клиенте у нас в
        дальнейшем никак не используется, поэтому
        вместо второго и третьего параметров можно
        было поставить значения NULL. */
        printf("Listen (accept)\n");
        if((newsockfd = accept(sockfd, (struct sockaddr *) &cliaddr, &clilen)) < 0)
        {
            perror(NULL);
            close(sockfd);
            exit(1);
        }

        /* Создаем для каждого запроса новый процесс */
        pid = fork();

        switch(pid)
        {
            case -1: /* Если при создании произошла ошибка */
                perror("Fork error");
                close(newsockfd);
                break;
            case 0: /* Выполнить в созданном (дочернем) процессе*/ 
                printf("OK, sleep 5 sec\n");
                sleep(5);

                /* Закрываем дескриптор главного сокета*/
                close(sockfd);

                /* Устанавливаем время ожидания для клиента при уже установленном соединении */
                if (setsockopt(newsockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
				    perror("Error to set timeout");
					
                if (setsockopt(newsockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
                    perror("Error to set timeout");

                /* В цикле принимаем информацию от клиента до
                тех пор, пока не произойдет ошибки (вызов read()
                вернет отрицательное значение) или клиент не
                закроет соединение (вызов read() вернет
                значение 0). Максимальную длину одной порции
                данных от клиента ограничим 999 символами. В
                операциях чтения и записи пользуемся дескриптором
                присоединенного сокета, т. е. значением, которое
                вернул вызов accept().*/
                while((n = read(newsockfd, line, 999)) > 0 )
                {
                    /* Печатаем принятый текст на экране */
                    printf("Received message: %s\n", line);

                    /* Принятые данные отправляем обратно */
                    if( (n = write(newsockfd, line, strlen(line) + 1)) < 0 )
                    {
                        perror(NULL);
                        close(newsockfd);
                        exit(1);
                    }
                }

                /* Если при чтении возникла ошибка – завершаем работу */
                if(n < 0)
                {
                    perror(NULL);
                    close(newsockfd);
                    exit(1);
                }

                /* Закрываем дескриптор присоединенного сокета и
                уходим ожидать нового соединения */
                close(newsockfd);

                exit(0); /* Завершаем дочерний процесс */
                break;
            default: /* Выполнить в родительском процессе */ 
                printf("New process pid: %d\n", pid);
                
                /* Закрываем дескриптор присоединенного сокета и
                уходим ожидать нового соединения */
                close(newsockfd);
                break;
        }
    }

    return 0;
}
