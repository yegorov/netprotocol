/* Простой пример UDP-сервера для сервиса echo */
// gcc UDPServer.c -o UDPServer -lpthread
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define COUNT_THREADS 3

struct param
{
    int* id; // id потока, показывает что он занят
    char* line; // строка для отправки
    int sockfd; // сокет
    struct sockaddr_in cliaddr; // адрес клиента
    int clilen; // длина адреса клиента
};

void * answer(void* arg) 
{
printf("OK, sleep 5\n");
sleep(5);
        struct param p = *((struct param *)arg);
        /* Печатаем принятый текст на экране */
        printf("%s\n", p.line);

        /* Принятый текст отправляем обратно по адресу отправителя */
        if(sendto(p.sockfd, p.line, strlen(p.line), 0, (struct sockaddr *) &(p.cliaddr), p.clilen) < 0)
        {
            //perror(NULL);
            //close(sockfd);
            //exit(1);
            printf("Error");
        } 
        free(p.line);
        *(p.id) = -42;
}

int main()
{

    pthread_t threads[COUNT_THREADS];
    int threadid[COUNT_THREADS] = { -42 };
    struct param p[COUNT_THREADS];
    char* l; /* Для передачи в поток */
    int i; /* Для цикла */

    int sockfd;      /* Дескриптор сокета */
    int clilen, n;   /* Переменные для различных длин и количества символов */
    char line[1000]; /* Массив для принятой и отсылаемой строки */
    struct sockaddr_in servaddr, cliaddr; /* Структуры для адресов сервера и клиента */

    /* Заполняем структуру для адреса сервера: семейство
    протоколов TCP/IP, сетевой интерфейс – любой, номер порта
    51000. Поскольку в структуре содержится дополнительное не
    нужное нам поле, которое должно быть нулевым, перед
    заполнением обнуляем ее всю */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(51000);
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* Создаем UDP сокет */
    if((sockfd = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror(NULL); /* Печатаем сообщение об ошибке */
        exit(1);
    }

    /* Настраиваем адрес сокета */
    if(bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
    {
        perror(NULL);
        close(sockfd);
        exit(1);
    }

    while(1) 
    {
        
        /* Основной цикл обслуживания*/
        /* В переменную clilen заносим максимальную длину
        для ожидаемого адреса клиента */
        clilen = sizeof(cliaddr);

printf("recvfrom\n");
        /* Ожидаем прихода запроса от клиента и читаем его.
        Максимальная допустимая длина датаграммы – 999
        символов, адрес отправителя помещаем в структуру
        cliaddr, его реальная длина будет занесена в
        переменную clilen */
        if((n = recvfrom(sockfd, line, 999, 0, (struct sockaddr *) &cliaddr, &clilen)) < 0)
        {
            perror(NULL);
            close(sockfd);
            exit(1);
        }

        l = (char*)malloc(strlen(line) * sizeof(char) + 1);
        strcpy(l, line);
        
        for(i = 0; i < COUNT_THREADS; ++i)
        {
            if((threadid[i] == -42 ) && 1)
            {
                threadid[i] = i;
                p[i].id = &threadid[i];
                p[i].line = l;
                p[i].sockfd = sockfd;
                p[i].cliaddr = cliaddr;
                p[i].clilen = clilen;
                
                pthread_create(&threads[i], NULL, answer, &p[i]);
                break;
            } 
        }

    }

    return 0;
}
