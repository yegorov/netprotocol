/* Простой пример UDP-сервера для сервиса echo 
для обслуживания нескольких клиентов */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
    int sockfd;      /* Дескриптор сокета */
    int clilen, n;   /* Переменные для различных длин и количества символов */
    char line[1000]; /* Массив для принятой и отсылаемой строки */
    struct sockaddr_in servaddr, cliaddr; /* Структуры для адресов сервера и клиента */
    pid_t pid; /* PID порожденного процесса */

    /* Заполняем структуру для адреса сервера: семейство
    протоколов TCP/IP, сетевой интерфейс – любой, номер порта
    51000. Поскольку в структуре содержится дополнительное не
    нужное нам поле, которое должно быть нулевым, перед
    заполнением обнуляем ее всю */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(51000);
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* Создаем UDP сокет */
    if((sockfd = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror(NULL); /* Печатаем сообщение об ошибке */
        exit(1);
    }

    /* Настраиваем адрес сокета */
    if(bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
    {
        perror(NULL);
        close(sockfd);
        exit(1);
    }

    while(1) 
    {
        /* Основной цикл обслуживания*/
        /* В переменную clilen заносим максимальную длину
        для ожидаемого адреса клиента */
        clilen = sizeof(cliaddr);

        /* Ожидаем прихода запроса от клиента и читаем его.
        Максимальная допустимая длина датаграммы – 999
        символов, адрес отправителя помещаем в структуру
        cliaddr, его реальная длина будет занесена в
        переменную clilen */
        printf("Listen (recvfrom)\n");
        if((n = recvfrom(sockfd, line, 999, 0, (struct sockaddr *) &cliaddr, &clilen)) < 0)
        {
            perror(NULL);
            close(sockfd);
            exit(1);
        }
        /* Создаем для каждого запроса новый процесс */
        pid = fork();
        switch(pid)
        {
            case -1: /* Если при создании произошла ошибка */
                perror("Fork error");
                break;
            case 0: /* Выполнить в созданном (дочернем) процессе*/ 
                printf("OK, sleep 5 sec\n");
                sleep(5);

                /* Печатаем принятый текст на экране */
                printf("Received message: %s\n", line);

                /* Принятый текст отправляем обратно по адресу отправителя */
                if(sendto(sockfd, line, strlen(line), 0, (struct sockaddr *) &(cliaddr), clilen) < 0)
                {
            	    perror("Error");
                    close(sockfd);
            	    exit(1);
                }
                exit(0); /* Завершаем дочерний процесс */
                break;
            default: /* Выполнить в родительском процессе */ 
                printf("New process pid: %d\n", pid);
                break;
        }
    }

    return 0;
}
